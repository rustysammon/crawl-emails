
This is a coding exercise from [jana.com]().

# Problem

Create a command line program that will take an internet domain name (i.e. “jana.com”) 
and print out a list of the email addresses that were found on that website.  For example:


```
> python find_email_addresses.py jana.com
Found these email addresses:
sales@jana.com
press@jana.com
info@jana.com
...
```

```
> python find_email_addresses.py mit.edu
Found these email addresses:
campus-map@mit.edu
mitgrad@mit.edu
sfs@mit.edu
llwebmaster@ll.mit.edu
webmaster@ll.mit.edu
whatsonyourmind@mit.edu
fac-officers@mit.edu
...
```

# Assumptions

1. We're looking for email addresses that appear *anywhere* in the web page, including visible text, mailto links, javascript, PDF files, etc.
2. Email addresses should only be listed once.  It is okay to assume that the username portion of an email address is case-insensitive.
3. You should list all emails, regardless of domain.
4. You should stay on the same subdomain when crawling the page.
  * Ex: If you search for a subdomain like `www.jana.com`, then this 
search would include pages like `www.jana.com/product` but would NOT include pages like `store.jana.com`
  * Ex: If you search for domain like `jana.com`, this this would search *ALL* subdomains, including `blog.jana.com` and `store.jana.com`.
5. When determining if a string is an email address, a standard email regex is fine.
6. You should respect the `robots.txt` for the domain.
7. You should ignore HTML schemes other than HTTP and HTTPS.
8. The domain that is provided in the command line is a valid one.

# Solution

This is a python 3 app (tested on Python 3.5.2).  

Install the requirements:

    pip install -r requirements.txt
    
Run the app on a subdomain:
    
    python crawl_email.py www.jana.com

Run the app on a domain. This takes much longer, since it includes crawling all of the subdomains of *jana.com*.

    python crawl_email.py jana.com
    
The output looks like:

```
Crawled 1644 web pages and found 6 email addresses
info@jana.com
sales@jana.com
something@mydomain.slack.com
moellman@jana.com
launch@jana.com
press@jana.com
```
    
    