import sys, requests, argparse, re
from urllib import parse, robotparser
from bs4 import BeautifulSoup

# When submitting http requests, identify ourselves as a bot
user_agent = 'JamesJanaInterviewbot/Rusty (https://gist.github.com/james-jana/ab930abbaa9409246fd2)'
user_agent_header = {'User-agent': user_agent}

def get_links(html, base_url, domain):
    """
    Get a list of all the links on a web page.  This function will filter out links that
    should not be crawled, such as anchors, links to other domains, etc.

    :param html: The html document, as a string.
    :param base_url: This base_url is used to expand relative URLs that are found in the html document
    :return: a list of URLs that that wer found in the page.
    """
    doc = BeautifulSoup(html, "lxml")
    links = [element.get('href') for element in doc.find_all('a')]

    new_links = []
    allowed_schemes = {'http', 'https'}
    for link in links:
        # Ignore empty links
        if not link:
            continue

        # Ignore anchors, since they're on the page that we've already crawled
        if link[0] == '#':
            continue

        # Convert relative URLs into full/absolute URLs
        link = parse.urljoin(base_url, link)

        # Ignore schemes other than HTTP
        parsed_url = parse.urlparse(link)
        if not parsed_url.scheme.lower() in allowed_schemes:
            continue

        # Ignore links that go outside of the specified domain
        # Include subdomains
        if not parsed_url.hostname.lower().endswith(domain):
            continue

        # If we get this far, then it's a valid link worth following
        new_links.append(link)
    links = new_links

    return links

def get_emails(text):
    """
    Find all the email addresses in a string.

    :param text: Any string.  Could be html, a text file, etc.
    :return: A list of email addresses that were found in the text
    """

    # This regexp was copied from:
    # http://emailregex.com/
    emails = re.findall(pattern="[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]*[a-zA-Z0-9-]",
                        string=text)
    return emails

def is_valid_domain(domain):
    """
    Is the specified domain formatted correctly to be a web domain?

    :param domain: a string like 'www.jana.com'
    :return: True if the domain appears to be a valid web domain.
        False if the domain does not appear to be a valid domain
    """

    # This regexp comes from:
    # http://stackoverflow.com/questions/10306690/domain-name-validation-with-regex
    return re.fullmatch(pattern='^(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\.[a-zA-Z]{2,3})$',
                        string=domain)

def main(argv):
    # Parse the command line arguments
    parser = argparse.ArgumentParser(description='This program will find all the email addresses on website.',
                                     usage='crawl_email.py domain')
    parser.add_argument("domain", help='The website domain. Crawling will begin at the top level of this domain '
                                       'and all links within the domain will be searched')
    args = parser.parse_args()

    # Check to make sure that the domain is correctly formatted
    # Odds are that the regexp in is_valid_domain() isn't perfect, so we treat this as a warning rather than an error
    args = parser.parse_args()
    if not is_valid_domain(args.domain):
        print('Warning: The string "{domain}" might not be a valid domain. But this program will try to crawl it anyway'.format(domain=args.domain))

    # Domains are case-insensitive. Scrub the user input to simplify comparisons later
    domain = args.domain.lower()

    # Check to see if there is a robots.txt file for this domain
    robots = robotparser.RobotFileParser()
    robots.set_url('http://' + domain + '/robots.txt')
    robots.read()

    # Starting with the top level page for this domain, find all the other pages and extract their email addresses
    links_todo = set()  # urls that we still need to visit
    links_done = set()  # urls that we have already visited (to avoid loops)
    links_todo.add('http://' + domain)
    emails_found = set()  # any email addresses that were found on the website
    while len(links_todo) != 0:
        try:
            # Fetch the next link and process it
            url = links_todo.pop()
            sys.stdout.write('\r{} pages done, {} pages in queue. Crawling web page: {}'.format(len(links_done), len(links_todo), url))
            sys.stdout.flush()

            links_done.add(url)
            response = requests.get(url, headers=user_agent_header)
            if response.status_code == 200:
                # Find the emails on the page
                emails = get_emails(response.text)
                # Standardize on all lowercase to avoid duplicates
                # Technically, the username portion of an email address could be case-sensitive,
                # but most hosts have case-insensitive email addresses
                emails = [email.lower() for email in emails]
                emails_found.update(emails)

                # Find any other links on the page
                links = get_links(response.text, url, domain)

                # Do some additional filtering to remove links that we shouldn't follow
                new_links = []
                for link in links:
                    # Remove links that are disallowed by the robots.txt file
                    if not robots.can_fetch(user_agent, link):
                        continue
                    # Remove links that we've already visited
                    if link in links_done or link in links_todo:
                        continue
                    # If we get this far, then it's a valid link worth following
                    new_links.append(link)
                links = new_links

                # Add these new links to our growing list of pages to crawl
                links_todo.update(links)
            else:
                print('Failed to load page with status code', response.status_code)
        except Exception as exception:
            print('\nFailure when processing page:', url, '\n', exception)

    # We're done crawling the web page.  Print the results
    print('\nCrawled {} web pages and found {} email addresses'.format(len(links_done), len(emails_found)))
    for email in emails_found:
        print(email)

if __name__ == "__main__":
    main(sys.argv[1:])